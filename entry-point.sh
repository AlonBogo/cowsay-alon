#!/bin/sh

if [ $# -eq 1 ];then
	export	PORT=$1
	echo "Used given ports $1 and $2"
else	
	echo "Used base ports 8080:8080"
fi
npm start
