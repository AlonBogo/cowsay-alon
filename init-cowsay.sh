#!/bin/bash

docker build -t mycowsay-alon .
if [ $# -eq 1 ];then
	docker run -d -p $1:$2 mycowsay-alon $2
else
	docker run -d -p 8080:8080 mycowsay-alon
fi